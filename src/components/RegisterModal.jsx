import React from 'react'
import { usePage } from '../hooks/usePage'
import reactDOM from 'react-dom'

export default function RegisterModal() {
    const { isOpenRegisterModal, setIsOpenRegisterModal } = usePage()

    return reactDOM.createPortal(
        <div className="popup-form popup-login" onClick={() => setIsOpenRegisterModal(false)} style={{ display: isOpenRegisterModal ? 'flex' : 'none' }}>
            <div className="wrap" onClick={ev => ev.stopPropagation()}>
                {/* login-form */}
                <div className="ct_login" style={{ display: 'block' }}>
                    <h2 className="title">Đăng ký</h2>
                    <input type="text" placeholder="Email / Số điện thoại" />
                    <input type="password" placeholder="Mật khẩu" />
                    <div className="remember">
                        <label className="btn-remember">
                            <div>
                                <input type="checkbox" />
                            </div>
                        </label>
                    </div>
                    <div className="btn rect main btn-login">đăng ký</div>
                </div>
            </div>
        </div>,
        document.body
    )
}
