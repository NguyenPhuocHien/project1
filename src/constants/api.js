import axios from "axios";
import { getToken, setToken } from "../utils/token";
import authService from '../services/auth'

const api = axios.create({
    baseURL: import.meta.env.VITE_HOST,
})

api.interceptors.response.use((res) => {

    // refresh token tự động khi hết hạn

    return res.data
}, async (error) => {

    // B1: Kiểm tra response error_code = TOKEN_EXPIRED
    if(error.response?.data?.error_code === 'TOKEN_EXPIRED') {
        let token = getToken()

        if(token) {
            // B2: lay refreshToken từ localStorage và tiến hành gọi api refreshToken, update accessToken vào localStorage
            const res = await authService.refreshToken({
                refreshToken: token.refreshToken
            })

            if(res.data) {
                token.accessToken = res.data.accessToken
                setToken(token)

                // B3: Run api bị thất bại
                return api(error.response.config)
            }
        }
    }

    throw error.response?.data

    /**
     * B1: Kiểm tra response error_code = TOKEN_EXPIRED
     * B2: lay refreshToken từ localStorage và tiến hành gọi api refreshToken, update accessToken vào localStorage
     * B3: Run api bị thất bại
     */
})

api.interceptors.request.use((config) => {
    // Găn token tự động vào header

    let token = getToken()
    if(token) {
        config.headers.Authorization = `Bearer ${token.accessToken}`
    }

    return config
})
export default api

