import React, { useEffect, useMemo } from 'react'

const $ = window.$

export default function Gallery() {

    const _id = useMemo(() => Math.floor(Math.random() * 10000000000))
    let id = `gallery-${_id}`


    useEffect(() => {
        let $carouselGallery = $(`#${id} .list`),
            $progressBar = $(`#${id} .timeline .process`);

        $carouselGallery.flickity({
            contain: true,
            wrapAround: false,
            freeScroll: true,
            cellAlign: 'left',
            lazyLoad: 3,
            imagesLoaded: true,
            prevNextButtons: false
        });

        $carouselGallery.on('scroll.flickity', function (event, progress) {
            progress = Math.max(0.05, Math.min(1, progress));
            $progressBar.width(progress * 100 + '%');
        });

        let ctrPrevGallery = $(`#${id} .btn_ctr.prev`),
            ctrNextGallery = $(`#${id} .btn_ctr.next`);

        ctrPrevGallery.on('click', function () {
            $carouselGallery.flickity('previous');
        });
        ctrNextGallery.on('click', function () {
            $carouselGallery.flickity('next');
        });
    }, [])

    return (
        <section className="section-gallery" id={id}>
            <div className="textbox">
                <h2 className="main-title">Chúng ta là một team</h2>
            </div>
            <div className="list">
                <img src="/img/img_team1.png" alt="" />
                <img src="/img/img_team2.png" alt="" />
                <img src="/img/img_team3.png" alt="" />
                <img src="/img/img_team4.png" alt="" />
                <img src="/img/img_team3.png" alt="" />
                <img src="/img/img_team4.png" alt="" />
                <img src="/img/img_team1.png" alt="" />
                <img src="/img/img_team2.png" alt="" />
                <img src="/img/img_team3.png" alt="" />
                <img src="/img/img_team4.png" alt="" />
                <img src="/img/img_team3.png" alt="" />
                <div className="item carousel-cell">
                    <img src="/img/img_team4.png" alt="" />
                </div>
            </div>
            <div className="controls">
                <div className="btn_ctr prev" />
                <span>Trượt qua</span>
                <div className="timeline">
                    <div className="process" />
                </div>
                <div className="btn_ctr next" />
            </div>
        </section>
    )
}
