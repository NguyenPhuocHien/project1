import { createStore, combineReducers, applyMiddleware, compose } from 'redux'
import { counterReducer } from './counterReducer'
import { authReducer } from './authReducer'
// import thunk from 'redux-thunk'


// action = {
//     type: 'counter/incremented',
//     payload: ...
// }


// const logging = (store) => {
//     return (next) => {
//         return (action) => {
//             next(action)
//         }
//     }
// }

const thunk = store => next => action => {
    if (typeof action === 'function') {
        action(store.dispatch)
        return
    }
    next(action)
}


const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION__ || compose

export let store = createStore(combineReducers({
    counter: counterReducer,
    auth: authReducer
}),
    applyMiddleware(
        // [
            // logging,
            thunk,
        // ]
        // window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
    )
)

// store.subscribe(() => console.log(store.getState()))

// setInterval(() => {
//     store.dispatch({ type: 'counter/incremented' })
// }, 1000)

// store.dispatch({ type: 'counter/incremented' })
// // // {value: 1}
// store.dispatch({ type: 'counter/incremented' })
// // {value: 2}
// store.dispatch({ type: 'counter/decremented' })
// // {value: 1}