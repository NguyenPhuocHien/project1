import { Link } from 'react-router-dom'
import { HOME_PATH, PROFILE_PATH } from '../constants/path'
import { useAuth } from '../hooks/useAuth'
import { usePage } from '../hooks/usePage'

export default function Header() {

    const { user, onLogin, onLogout } = useAuth()
    const { setIsOpenLoginModal, setIsOpenRegisterModal } = usePage()

    const toggleSidebar = () => {
        document.body.classList.toggle('menu-is-show')
    }

    const _onLogout = (ev) => {
        ev.preventDefault()
        onLogout()
    }
    return (
        <header id="header">
            <div className="wrap">
                <div className="menu-hambeger" onClick={toggleSidebar}>
                    <div className="button">
                        <span />
                        <span />
                        <span />
                    </div>
                    <span className="text">menu</span>
                </div>
                <Link to={HOME_PATH} className="logo">
                    <img src="/img/logo.svg" alt="" />
                    <h1>CFD</h1>
                </Link>
                <div className="right">
                    {
                        user ? (
                            <div className="have-login">
                                <div className="account">
                                    <a href="#" className="info">
                                        <div className="name">{user.name}</div>
                                        <div className="avatar">
                                            <img src={user.avatar} alt="" />
                                        </div>
                                    </a>
                                </div>
                                <div className="hamberger">
                                </div>
                                <div className="sub">
                                    <a href="#">Khóa học của tôi</a>
                                    <Link to={PROFILE_PATH}>Thông tin tài khoản</Link>
                                    <a href="#" onClick={_onLogout}>Đăng xuất</a>
                                </div>
                            </div>
                        ) : (
                            <div className="not-login bg-none">
                                <a href="#" className="btn-register" onClick={(ev) => {
                                    ev.preventDefault()
                                    setIsOpenLoginModal(true)
                                }}>Đăng nhập</a>
                                <a href="#" className="btn main btn-open-login" onClick={(ev) => {
                                    ev.preventDefault()
                                    setIsOpenRegisterModal(true)
                                }}>Đăng ký</a>
                            </div>
                        )
                    }

                    {/* <div class="not-login bg-none">
                    <a href="#" class="btn-register">Đăng nhập</a>
                    <a href="login.html" class="btn main btn-open-login">Đăng ký</a>
                </div> */}
                </div>
            </div>
        </header>
    )
}
