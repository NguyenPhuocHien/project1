import React, { useRef } from 'react'
import { useState } from 'react'
import styled from 'styled-components'
import { useSelector, useDispatch } from 'react-redux'
import { decrementAction, incrementAction } from '../stores/counterReducer'

const ButtonStyle = styled.button`
    padding: 10px 20px;
    font-size: 18px;
    margin-right: 20px;
`

const CounterWrap = styled.div`
    font-size: 50px;
    display: flex;
    gap: 40px;
`
export default function Demo() {
    const [random, setRandom] = useState()
    // const [count, setCount] = useState(1)
    // const state = useSelector((store) => store.counter)

    const count = useRef(0)
    // const dispatch = useDispatch()
    console.log('re-render demo')

    return (
        <CounterWrap style={{ margin: 100 }}>
            <button onClick={() => count.current--}>Giam</button>
            {count.current}
            <button onClick={() => count.current++}>Tang</button>

            <button onClick={() => setRandom(Math.random())}>Re render</button>
        </CounterWrap>
    )
}



export const ButtonA = ({ count, setCount, children }) => {
    return <ButtonStyle onClick={() => setCount(count + 1)}>{children}: {count}</ButtonStyle>
}



export const ButtonB = ({ count, setCount }) => {
    return <ButtonStyle onClick={() => setCount(count + 1)}>B: {count}</ButtonStyle>
}