import React, { useRef } from 'react'
import { useEffect } from 'react'
import { useState } from 'react'
import Input from '../components/Input'
import validate, { pattern, required } from '../utils/validate'

export default function Contact() {
    const [value, setValue] = useState({})
    const [error, setError] = useState({})
    const inputRef = useRef(10)
    console.log(inputRef)
    useEffect(() => {
        inputRef.current.focus()
    }, [])
    const onSubmit = (ev) => {
        ev.preventDefault()

        const error = validate(value, {
            name: [
                required()
            ],
            phone: [
                required(),
                pattern('phone')
            ],
            email: [
                required(),
                pattern('email')
            ],
            website: [
                required(),
                pattern('url')
            ],
            title: [
                required()
            ]
        })
        setError(error)
        if (Object.keys(error).length === 0) {
            alert('Thanh cong')
        }
    }

    return (
        <main className="register-course" id="main">
            <section className="section-1 wrap container">
                {/* <div class="main-sub-title">liên hệ</div> */}
                <h2 className="main-title">HỢP TÁC CÙNG CFD</h2>
                <p className="top-des">
                    Đừng ngần ngại liên hệ với <strong>CFD</strong> để cùng nhau tạo ra những sản phẩm giá trị, cũng như
                    việc hợp tác với các đối tác tuyển dụng và công ty trong và ngoài nước.
                </p>
                <form className="form" onSubmit={onSubmit}>
                    <input type="text" ref={inputRef} />
                    <Input
                        required
                        label="Họ và tên"
                        placeholder="Họ và tên bạn"
                        // onChange={ev => value.name = ev.target.value}
                        error={error.name}
                    />
                    <Input
                        required
                        label="Số điện thoại"
                        placeholder="Số điện thoại"
                        onChange={ev => value.phone = ev.target.value}
                        error={error.phone}
                    />
                    <Input
                        required
                        label="Email"
                        placeholder="Email của bạn"
                        onChange={ev => value.email = ev.target.value}
                        error={error.email}
                    />
                    <Input
                        required
                        label="Website"
                        placeholder="Đường dẫn website http://"
                        onChange={ev => value.website = ev.target.value}
                        error={error.website}
                    />
                    <Input
                        required
                        label="Tiêu đề"
                        placeholder="Tiêu đề liên hệ"
                        onChange={ev => value.title = ev.target.value}
                        error={error.title}
                    />
                    <label>
                        <p>Nội dung</p>
                        <textarea onChange={ev => value.content = ev.target.value} name id cols={30} rows={10} defaultValue={""} />
                    </label>
                    <button className="btn main rect">đăng ký</button>
                </form>
            </section>
            {/* <div class="register-success">
            <div class="contain">
                <div class="main-title">đăng ký thành công</div>
                <p>
                    <strong>Chào mừng Trần Nghĩa đã trở thành thành viên mới của CFD Team.</strong> <br>
                    Cảm ơn bạn đã đăng ký khóa học tại <strong>CFD</strong>, chúng tôi sẽ chủ động liên lạc với bạn thông qua facebook
                    hoặc số điện thoại của bạn.
                </p>
            </div>
            <a href="/" class="btn main rect">về trang chủ</a>
        </div> */}
        </main>
    )
}
