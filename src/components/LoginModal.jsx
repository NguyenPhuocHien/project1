import { useState } from 'react'
import reactDOM from 'react-dom'
import { useDispatch } from 'react-redux'
import styled from 'styled-components'
import { useAuth } from '../hooks/useAuth'
import { usePage } from '../hooks/usePage'
import { loginAction } from '../stores/authReducer'
import validate, { pattern, required } from '../utils/validate'
const ErrorMessage = styled.p`
    color: red;
`

export default function LoginModal() {
    const { isOpenLoginModal, setIsOpenLoginModal } = usePage()
    const [value, setValue] = useState({})
    const [error, setError] = useState({})
    const [errorMessage, setErrorMessage] = useState('')
    const { setUser } = useAuth()
    const dispatch = useDispatch()

    const onSubmit = async (ev) => {
        ev.preventDefault()

        setErrorMessage('')
        const error = validate(value, {
            username: [
                required('Username không được để trống'),
                pattern('email', 'Vui lòng điền đúng định dạng email')
            ],
            password: [
                required('Xin vui lòng điền password'),
                // pattern('password', 'Xin vui lòng điền password với các từ viết hoa, thường, số, ký tự đặc biệt và từ 6-32 ký tự')
            ]
        })

        setError(error)
        if (Object.keys(error).length === 0) {
            dispatch(loginAction({
                payload: value,
                success: () => {
                    setIsOpenLoginModal(false)
                },
                error: (error) => {
                    if (error.message) {
                        setErrorMessage(error.message)
                    }
                }
            }))
        }



        // try {
        //     setErrorMessage('')
        //     const res = await authService.login(value)
        //     localStorage.setItem('login', JSON.stringify(res.data))

        //     const user = await userService.getInfo()

        //     setUser(user.data)

        //     localStorage.setItem('user', JSON.stringify(user.data))
        //     setIsOpenLoginModal(false)

        // } catch (error) {
        //     if (error.response.data.message) {
        //         setErrorMessage(error.response.data.message)
        //     }

        // }
    }

    return reactDOM.createPortal(
        <div className="popup-form popup-login" onClick={() => setIsOpenLoginModal(false)} style={{ display: isOpenLoginModal ? 'flex' : 'none' }}>
            <div className="wrap" onClick={ev => ev.stopPropagation()}>
                {/* login-form */}
                <form className="ct_login" style={{ display: 'block' }} onSubmit={onSubmit}>
                    <h2 className="title">Đăng nhập</h2>
                    {
                        errorMessage && <ErrorMessage>{errorMessage}</ErrorMessage>
                    }
                    <input onChange={ev => value.username = ev.target.value} type="text" placeholder="Email / Số điện thoại" />
                    {
                        error.username && <ErrorMessage>{error.username}</ErrorMessage>
                    }
                    <input onChange={ev => value.password = ev.target.value} type="password" placeholder="Mật khẩu" />
                    {
                        error.password && <ErrorMessage>{error.password}</ErrorMessage>
                    }
                    <div className="remember">
                        <label className="btn-remember">
                            <div>
                                <input type="checkbox" />
                            </div>
                            <p>Nhớ mật khẩu</p>
                        </label>
                        <a href="#" className="forget">Quên mật khẩu?</a>
                    </div>
                    <button type="submit" className="btn rect main btn-login">đăng nhập</button>
                </form>
            </div>
        </div>,
        document.body
    )
}
