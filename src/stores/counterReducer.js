
const initialState = { value: 0 }

const INCREMENT = 'counter/incremented'
const DECREMENT = 'counter/decremented'


export const incrementAction = (count = 1) => ({ type: INCREMENT, payload: count })
export const decrementAction = () => ({ type: DECREMENT })

export function counterReducer(state = initialState, action) {
    console.log('counter', state, action)
    switch (action.type) {
        case INCREMENT:
            return { value: state.value + action.payload }
        case DECREMENT:
            return { value: state.value - 1 }
        default:
            return state
    }

}