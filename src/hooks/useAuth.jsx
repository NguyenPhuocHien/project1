import { useEffect } from "react"
import { createContext, useContext, useState } from "react"
import userService from "../services/user"
import { clearToken } from "../utils/token"
import { useSelector, useDispatch } from 'react-redux'

const AuthContext = createContext({ a: 100 })

let token = localStorage.getItem('login')
if (token) {
    token = JSON.parse(token)
}



export const AuthProvider = ({ children }) => {
    const [user, setUser] = useState(() => {
        let _user = localStorage.getItem('user')
        if (_user) {
            _user = JSON.parse(_user)
        }
        return _user
    })
    useEffect(() => {
        (async () => {
            if (token) {
                const user = await userService.getInfo()

                if (user.data) {
                    setUser(user.data)
                }
            }
        })()
    }, [])

    useEffect(() => {
        if (user) {
            localStorage.setItem('user', JSON.stringify(user))
        }else {
            localStorage.removeItem('user')
            clearToken()
        }
    }, [user])




    const onLogout = () => {
        setUser()
    }

    return <AuthContext.Provider value={{ user, setUser, onLogout }}>{children}</AuthContext.Provider>
}


// export const useAuth = () => useContext(AuthContext)


export const useAuth = () => {
    const { user } = useSelector(store => store.auth)
    const dispatch = useDispatch()

    const setUser = (data) => {
        dispatch({ type: 'auth/setUser', payload: data })
    }

    const onLogout = () => {
        dispatch({ type: 'auth/logout' })
        localStorage.clear('user')
        localStorage.clear('login')
    }

    return { user, setUser, onLogout }
}


