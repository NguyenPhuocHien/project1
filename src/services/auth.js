import api from "../constants/api"

export const authService = {
    login(data){
        return api.post('/login', data)
    },
    register(data){
        return api.post('/register', data)
    },
    refreshToken(data) {
        return api.post('/refresh-token', data)
    }
}

export default authService